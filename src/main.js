import { model, FALLBACKS } from './model.js'
import { clumpingPlot, orderPlot } from './plots.js'
import { uiSetup } from './ui.js'
import { $ } from './util.js'

uiSetup(model, orderPlot, clumpingPlot)

const params = Object.fromEntries(
    new URL(document.location).searchParams.entries()
)
// WARNING! special case
if (params.W) params.H = params.W
// sanitize
for (const key of Object.keys(params))
    if (!FALLBACKS.hasOwnProperty(key)) delete params[key]

model.setup($('#main-canvas'), params)
model.setSize()
model.update()
