import { $ } from './util.js'

export const uiSetup = (model, orderPlot, clumpingPlot) => {
    $('#plotbox0').appendChild(orderPlot.uplot.ctx.canvas)
    $('#plotbox1').appendChild(clumpingPlot.uplot.ctx.canvas)

    $('#main-canvas').title = 'click to run/pause animation'

    $('#main-canvas').addEventListener('click', model.toggleRun.bind(model))
    window.addEventListener('resize', model.setSize.bind(model))
    window.addEventListener('resize', () => {
        const parent = $('.plotbox')
        const size = {
            width: parent.clientWidth,
            height: parent.clientHeight,
        }
        for (const p of [orderPlot, clumpingPlot]) {
            p.uplot.setSize(size)
        }
    })

    model.addEventListener('statsEvent', () => {
        $('#hz').textContent = (model.hz || 0).toFixed()
        $('#order-parameter').textContent = model.getOrderParameter().toFixed(3)
        orderPlot.addData(model.totalUpdates, model.getOrderParameter())
        clumpingPlot.addData(model.totalUpdates, model.getClumpingParameter())
        $('#clumping-parameter').textContent = model
            .getClumpingParameter()
            .toFixed(3)
    })
    model.addEventListener('configEvent', () => {
        model.totalUpdates = 0
        orderPlot.setData((orderPlot.data = [[0], [model.getOrderParameter()]]))
        clumpingPlot.setData((clumpingPlot.data = [[], []]))

        document.title = `${model.N} particles moving somewhat randomly`
        $('#number-value').textContent = $('#number').value = model.N
        $('#width-value').textContent = $('#width').value = model.W
        $('#density-value').textContent = (model.N / model.W / model.H).toFixed(3)
        $('#noise-value').textContent = $('#noise').value = model.noise
        $('#speed-value').textContent = $('#speed').value = model.speed
        $('#particle-size-value').textContent = $('#particle-size').value =
            model.pointSize
        $('#paint-trails').checked = model.paintTrails
        $('#order-parameter').textContent = model.getOrderParameter().toFixed(3)
        $('#clumping-parameter').textContent = '?'
    })
    $('#number').addEventListener('input', ev => {
        const N = ev.target.value
        $('#number-value').textContent = N
        $('#density-value').textContent = (N / model.W / model.H).toFixed(3)
    })
    $('#number').addEventListener('change', ev => {
        model.setNumber(ev.target.value)
        model.update()
    })
    $('#width').addEventListener('input', ev => {
        const W = ev.target.value
        $('#width-value').textContent = W
        $('#density-value').textContent = (model.N / W / W).toFixed(3)
    })
    $('#width').addEventListener('change', ev => {
        const W = ev.target.value
        model.setDimensions(W, W)
        model.update()
    })
    $('#noise').addEventListener('input', ev => {
        $('#noise-value').textContent = ev.target.value
        model.setNoise(ev.target.value)
    })
    $('#speed').addEventListener('input', ev => {
        $('#speed-value').textContent = ev.target.value
        model.setSpeed(ev.target.value)
    })
    $('#particle-size').addEventListener('input', ev => {
        $('#particle-size-value').textContent = ev.target.value
        model.setPointSize(ev.target.value)
    })
    $('#noninteracting').addEventListener('input', ev => {
        model.interacting = !ev.target.checked
    })
    $('#paint-trails').addEventListener('input', ev =>
        model.setTrails(ev.target.checked)
    )
    $('#randomize').addEventListener('click', ev => {
        model.randomize()
        model.update()
    })
    $('#reset').addEventListener('click', ev => {
        model.stop()
        localStorage.clear()
        const url = new URL(location.href)
        location.href = url.origin + url.pathname
    })
    $('#current-params').addEventListener('click', ev => {
        const params = model.getParams()
        const query = new URLSearchParams()
        for (const [k, v] of Object.entries(params)) query.append(k, v)
        const url = new URL(document.location)
        url.search = query.toString()
        navigator.clipboard
            .writeText(url)
            .then(() => {
                const text = $('#current-params').innerHTML
                $('#current-params').innerHTML = 'LINK COPIED TO CLIPBOARD'
                setTimeout(() => {
                    $('#current-params').innerHTML = text
                }, 3000)
            })
            .catch(() => alert('copying to clipboard failed (?)'))
    })
}
