import uPlot from 'uplot'
import { $ } from './util.js'

export const orderPlot = {
    container: $('#plotbox0'),
    data: [[], []],
    counts: [],
    uplot: new uPlot({
        width: $('#plotbox0').clientWidth,
        height: $('#plotbox0').clientHeight,
        padding: [null, null, 5, null],
        axes: [
            { show: false },
            {
                show: true,
                size: 20,
                gap: 0,
                stroke: '#aaaaaa',
                font: '10px Arial',
                grid: {
                    show: true,
                    stroke: '#404040',
                    dash: [],
                    width: 1,
                },
                ticks: {
                    show: false,
                    size: 0,
                },
            },
        ],
        scales: {
            x: {
                time: false,
                auto: false,
                range: [0, 6000],
            },
            y: {
                auto: false,
                range: [0, 1],
            },
        },
        series: [
            {},
            {
                show: true,
                value: 'time series',
                width: 1,
                stroke: '#aaaaaa',
                fill: '#7f7f7f7f',
                points: { show: false },
            },
        ],
    }),
    addData(count, val) {
        const [X, Y] = this.data
        X.push(count)
        Y.push(val)
        if (count - X[0] > 6000) {
            X.shift()
            Y.shift()
        }
        this.uplot.setData([X.map((_, i) => X[i] - X[0]), Y])
        $('#plot-label').innerText = `last ${count - X[0]} steps of ${count}`
    },
    setData(data) {
        this.data = data
        this.uplot.setData(data)
        $(
            '#plot-label'
        ).innerText = `last ${data.length} steps of ${data.length}`
    },
}

export const clumpingPlot = {
    container: $('#plotbox1'),
    data: [[], []],
    counts: [],
    uplot: new uPlot({
        width: $('#plotbox1').clientWidth,
        height: $('#plotbox1').clientHeight,
        padding: [null, null, 5, null],
        axes: [
            { show: false },
            {
                show: true,
                size: 20,
                gap: 0,
                stroke: '#aaaaaa',
                font: '10px Arial',
                grid: {
                    show: true,
                    stroke: '#404040',
                    dash: [],
                    width: 1,
                },
                ticks: {
                    show: false,
                    size: 0,
                },
            },
        ],
        scales: {
            x: {
                time: false,
                auto: false,
                range: [0, 6000],
            },
            y: {
                range: {
                    min: { soft: 0, mode: 1 },
                    max: {
                        soft: 10,
                        mode: 1,
                        pad: 0,
                    },
                }, //[0, 10],
            },
        },
        series: [
            {},
            {
                show: true,
                value: 'time series',
                width: 1,
                stroke: '#aaaaaa',
                fill: '#7f7f7f7f',
                points: { show: false },
            },
        ],
    }),
    addData(count, val) {
        const [X, Y] = this.data
        X.push(count)
        Y.push(val)
        if (count - X[0] > 6000) {
            X.shift()
            Y.shift()
        }
        this.uplot.setData([X.map((_, i) => X[i] - X[0]), Y])
        // $('#plot-label').innerText = `last ${count - X[0]} steps of ${count}`
    },
    setData(data) {
        this.data = data
        this.uplot.setData(data)
        // $(
        //     '#plot-label'
        // ).innerText = `last ${data.length} steps of ${data.length}`
    },
}

orderPlot.uplot.ctx.canvas.style.height = $('#plotbox0').clientHeight + 'px'
clumpingPlot.uplot.ctx.canvas.style.height = $('#plotbox1').clientHeight + 'px'
orderPlot.uplot.ctx.canvas.style.width = $('#plotbox0').clientWidth + 'px'
clumpingPlot.uplot.ctx.canvas.style.width = $('#plotbox1').clientWidth + 'px'