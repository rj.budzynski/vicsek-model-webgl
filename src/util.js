export const $ = document.querySelector.bind(document)

export const storageAvailable = (storageType) => {
    try {
        const storage = window[storageType]
        const x = '__storage_test__'
        storage.setItem(x, x)
        storage.removeItem(x)
        return true
    } catch(e) {
        console.log(e)
        return false
    }
}
