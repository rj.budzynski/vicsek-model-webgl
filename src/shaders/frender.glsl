#version 300 es
precision highp float;
in vec4 v_color;
out vec4 fragColor;

void main() {
    float theta = v_color.a;
    float c = cos(theta), s = sin(theta);
    float x = c*(2.*gl_PointCoord.x - 1.) - s*(2.*gl_PointCoord.y - 1.);
    float y = s*(2.*gl_PointCoord.x - 1.) + c*(2.*gl_PointCoord.y - 1.);
    // fragColor = vec4(v_color.rgb, 1.)
    //       * clamp(0., 1., (
    //         step(0., -y - .25*x)
    //       * step(0., -.25*x + y)
    //       * smoothstep(-1., -.6, x)
    //       + step(-.04, -x*x-y*y)
    //         )
    //       )
    fragColor = vec4(v_color.rgb, 1.)
        * step(-.25, -y - .25 * x)
        * step(-.25, y - .25 * x)
        * step(-1. , -x*x - y*y)
        //  + step(-1., -x*x-y*y)*step(.94, x*x+y*y) * vec4(1., 1., 1., .4)
    ;
}