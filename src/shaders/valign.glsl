#version 300 es
#define TWOPI radians(360.)
#define TEXTURE_WIDTH MAX_TEXTURE_SIZE
precision highp float;
precision highp int;
uniform int N;
uniform sampler2D data;
uniform highp usampler2D offsets;
uniform vec2 size;
uniform float noise;
uniform float seed;
out vec4 v_data;

#pragma glslify: random = require('./random.glsl')

ivec2 ind(int k) {
    return ivec2(k % TEXTURE_WIDTH, k / TEXTURE_WIDTH);
}

void main() {
    int W = int(size.x), H = int(size.y);
    ivec2 dataSize = textureSize(data, 0);
    vec4 in_vert = texelFetch(data, ind(gl_VertexID), 0);
    vec2 pos = in_vert.xy;
    float theta = in_vert.z;

    // THE ACTUAL ALIGNMENT HAPPENS HERE
    int thisCell = int(in_vert.w);
    int[9] neighborCells = int[](thisCell, thisCell+1, thisCell+W+1,
        thisCell+W, thisCell+W-1, thisCell-1, thisCell-W-1, thisCell-W,
        thisCell-W-1);
    float s = 0., c = 0.;
    int n = 0;
    for(int i = 0; i < 9; ++i) {
        float x = pos.x, y = pos.y;
        int cell = neighborCells[i];
        int cellX = cell % W, cellY = cell / W;
        if (cellX < 0){ cellX = W - 1; x += float(W); }
        if (cellX == W){ cellX = 0; x -= float(W); }
        if (cellY < 0){ cellY = H - 1; y += float(H); }
        if (cellY == H){ cellY = 0; y -= float(H); }
        int offset = int(texelFetch(offsets, ind(cell), 0).r);
        while (offset < N){
            vec4 vert = texelFetch(data, ind(offset), 0);
            if (int(vert.w) != cell) break;
            if (distance(vert.xy, vec2(x, y)) < 1.){
                ++n;
                float th = vert.z;
#ifdef NEMATIC
                float angle = theta - th;
                float signFactor = sign(cos(angle));
                s += signFactor * sin(th);
                c += signFactor * cos(th);
#else
                s += sin(th);
                c += cos(th);
#endif
            }
            ++offset;
        }
    }
    s /= float(n);
    c /= float(n);

    theta = atan(s, c);
    // END OF ALIGNMENT PROCEDURE

    theta += (random(seed * pos) - .5) * TWOPI * noise;
    // in_vert.w is the cell number, no longer needed since it's about 
    // to be recomputed anyway
    // meanwhile `n` can be useful for computing various order parameters
    // v_data = vec4(pos, theta, in_vert.w);

    v_data = vec4(pos, theta, float(n));
}