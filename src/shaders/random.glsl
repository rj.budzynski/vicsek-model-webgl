#define MANTISSA_MASK 0x007FFFFFu
#define FLOAT_ONE     0x3F800000u

uint hash(uvec2 xy) {
    uint x = xy.x;
    uint y = xy.y;
    x += x >> 11;
    x ^= x << 7;
    x += y;
    x ^= x << 6;
    x += x >> 15;
    x ^= x << 5;
    x += x >> 12;
    x ^= x << 9;
    return x;
}

float random(vec2 uv) {
    uvec2 bits = floatBitsToUint(uv);
    uint h = hash(bits);
    h &= MANTISSA_MASK;
    h |= FLOAT_ONE;

    float r2 = uintBitsToFloat(h);
    return r2 - 1.;
}
#pragma glslify: export(random)