#version 300 es
#define TWOPI radians(360.)
#define PI radians(180.)
precision highp float;
uniform vec2 size;
uniform float speed;
uniform float pointSize;
in vec4 in_data;
out vec4 out_data;
out vec4 v_color;

vec3 hsv_to_rgb(vec3 hsv) {
    float v = hsv.z;
    if (hsv.y == 0.) return vec3(v, v, v);
    float f = fract(6. * hsv.x);
    float p = v * (1. - hsv.y);
    float q = v * (1. - f * hsv.y);
    float t = v * (1. - hsv.y * (1. - f));
    int i = int(floor(6. * hsv.x)) % 6; 
    switch (i) {
        case 0: return vec3(v, t, p);
        case 1: return vec3(q, v, p);
        case 2: return vec3(p, v, t);
        case 3: return vec3(p, q, v);
        case 4: return vec3(t, p, v);
        case 5: return vec3(v, p, q);
    }
}

void main() {
    vec2 pos = in_data.xy;
    float theta = in_data.z;
    gl_Position = vec4(2. * pos / size - 1., 0., 1.);
    pos += speed * vec2(cos(theta), sin(theta));
    // periodic b. c. 
    pos = fract((pos + size) / size) * size;
    // reflecting walls
    // if (pos.x < 0.) {pos.x = -pos.x; theta = PI - theta;}
    // if (pos.x > size.x) {pos.x = 2.*size.x - pos.x; theta = PI - theta;}
    // if (pos.y < 0.) {pos.y = -pos.y; theta = -theta;}
    // if (pos.y > size.y) {pos.y = 2.*size.y - pos.y; theta = -theta;}

    float cell = floor(pos.x) + size.y * floor(pos.y);
    out_data = vec4(pos, theta, cell);
    float hue = fract((theta < 0. ? theta + TWOPI : theta) / TWOPI);
    v_color = vec4(hsv_to_rgb(vec3(hue, 1., 1.)), theta);
    gl_PointSize = pointSize;
}