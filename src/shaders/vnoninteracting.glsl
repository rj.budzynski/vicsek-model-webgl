#version 300 es
#define TWOPI radians(360.)
#define TEXTURE_WIDTH MAX_TEXTURE_SIZE
precision highp float;
precision highp int;
uniform int N;
uniform vec2 size;
uniform float noise;
uniform float seed;
uniform sampler2D data;
uniform highp usampler2D offsets;
out vec4 v_data;

#pragma glslify: random = require('./random.glsl')

ivec2 idx(int k) {
    return ivec2(k % TEXTURE_WIDTH, k / TEXTURE_WIDTH);
}

void main() {
    int W = int(size.x), H = int(size.y);
    ivec2 dataSize = textureSize(data, 0);
    vec4 in_vert = texelFetch(data, idx(gl_VertexID), 0);
    vec2 pos = in_vert.xy;
    float theta = in_vert.z;

    int thisCell = int(in_vert.w);
    int[9] neighborCells = int[](thisCell, thisCell+1, thisCell+W+1,
        thisCell+W, thisCell+W-1, thisCell-1, thisCell-W-1, thisCell-W,
        thisCell-W-1);

    int n = 0;
    for(int i = 0; i < 9; ++i) {
        float x = pos.x, y = pos.y;
        int cell = neighborCells[i];
        int cellX = cell % W, cellY = cell / W;
        if (cellX < 0){ cellX = W - 1; x += float(W); }
        if (cellX == W){ cellX = 0; x -= float(W); }
        if (cellY < 0){ cellY = H - 1; y += float(H); }
        if (cellY == H){ cellY = 0; y -= float(H); }
        int offset = int(texelFetch(offsets, idx(cell), 0).r);
        while (offset < N){
            vec4 vert = texelFetch(data, idx(offset), 0);
            if (int(vert.w) != cell) break;
            if (distance(vert.xy, vec2(x, y)) < 1.) ++n;
            ++offset;
        }
    }

    theta += (random(seed * pos) - .5) * TWOPI * noise;
    v_data = vec4(pos, theta, float(n));
}