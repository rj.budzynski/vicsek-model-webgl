import { PicoGL } from 'picogl'
import { storageAvailable } from './util.js'
import vmoveAndRender from './shaders/vmoveAndRender.glsl'
import frender from './shaders/frender.glsl'
import vertex from './shaders/vertex.glsl'
import ffade from './shaders/ffade.glsl'
import valign from './shaders/valign.glsl'
import fnoop from './shaders/fnoop.glsl'
import vnoninteracting from './shaders/vnoninteracting.glsl'

const { min, max, random, PI, sin, cos, hypot } = Math
const TWOPI = 2 * PI

const useStorage = storageAvailable('localStorage')
const statsEvent = new CustomEvent('statsEvent')
const configEvent = new CustomEvent('configEvent')

export const FALLBACKS = {
    N: 49999,
    W: 320,
    H: 320,
    speed: 0.3,
    noise: 0.17,
    interacting: true,
    pointSize: 0.6,
    fadeAlpha: 0.04,
    updateInterval: 25,
    statsUpdateInterval: 1000,
    paintTrails: true,
}

export const model = Object.assign(new EventTarget(), FALLBACKS)

if (useStorage) {
    for (const k of Object.keys(FALLBACKS)) {
        let val = localStorage.getItem(k)
        if (k == 'paintTrails') val = !(val === 'false') // KLUDGE!!!
        if (val !== null && val !== undefined) model[k] = val
    }
}

Object.assign(model, {
    getParams() {
        const params = {}
        for (const [k, v] of Object.entries(FALLBACKS))
            if (this[k] !== v) params[k] = this[k]
        return params
    },

    setup(canvas, params = {}) {
        // params come from the url and override localStorage
        Object.assign(this, params)
        // const canvas = $('#main-canvas')
        const { N, W, H, speed, pointSize } = this

        const app = PicoGL.createApp(canvas, {
            depth: false,
            alpha: false,
            premultipliedAlpha: false,
            preserveDrawingBuffer: true,
        })
            .clearColor(0, 0, 0, 1)
            .clear()

        const MAX_TEXTURE_SIZE = (this.MAX_TEXTURE_SIZE = app.gl.getParameter(
            app.gl.MAX_TEXTURE_SIZE
        ))
        console.log({ MAX_TEXTURE_SIZE })

        const moveAndRenderProgram = app.createProgram(
            vmoveAndRender,
            frender,
            {
                transformFeedbackVaryings: ['out_data'],
            }
        )

        const vertexBuffers = Array.from({ length: 2 }, () =>
            app.createVertexBuffer(PicoGL.FLOAT, 4, 4 * N, PicoGL.DYNAMIC_DRAW)
        )

        const vertexArray = app
            .createVertexArray()
            .vertexAttributeBuffer(0, vertexBuffers[0])

        const drawCall = app
            .enable(PicoGL.BLEND)
            .blendFunc(PicoGL.SRC_ALPHA, PicoGL.ONE_MINUS_SRC_ALPHA)
            .createDrawCall(moveAndRenderProgram, vertexArray)
            .uniform('size', [W, H])
            .uniform('speed', speed)
            .uniform('pointSize', (pointSize * canvas.width) / W)
            .primitive(PicoGL.POINTS)
            .drawRanges([0, N])
            .transformFeedback(
                app
                    .createTransformFeedback()
                    .feedbackBuffer(0, vertexBuffers[1])
            )

        const dataArray = this.initialConditions()
        vertexBuffers[0].data(dataArray.subarray(0, 4 * N))

        const fadeCall = app
            .createDrawCall(app.createProgram(vertex, ffade))
            .uniform('fadeAlpha', this.fadeAlpha)
            .primitive(PicoGL.TRIANGLE_FAN)
            .drawRanges([0, 4])

        const dataTextureHeight = ((N / MAX_TEXTURE_SIZE) | 0) + 1
        const dataTexture = app.createTexture2D(
            MAX_TEXTURE_SIZE,
            dataTextureHeight,
            {
                internalFormat: PicoGL.RGBA32F,
                minFilter: PicoGL.NEAREST,
                magFilter: PicoGL.NEAREST,
            }
        )

        // const offsetsTextureWidth = min(8192, W * H)
        const offsetsTextureHeight = (((W * H) / MAX_TEXTURE_SIZE) | 0) + 1
        const offsetsTexture = app.createTexture2D(
            MAX_TEXTURE_SIZE,
            offsetsTextureHeight,
            {
                internalFormat: PicoGL.R32UI,
                minFilter: PicoGL.NEAREST,
                magFilter: PicoGL.NEAREST,
            }
        )
        const offsetsArray = new Uint32Array(
            offsetsTexture.width * offsetsTexture.height
        ) // .fill(0xffffffff)

        const alignProgram = app.createProgram(
            valign.replaceAll('MAX_TEXTURE_SIZE', MAX_TEXTURE_SIZE),
            fnoop,
            {
                transformFeedbackVaryings: ['v_data'],
            }
        )

        const alignCall = app
            .createDrawCall(alignProgram, app.createVertexArray())
            .uniform('N', N)
            .uniform('size', [W, H])
            .uniform('noise', this.noise)
            .texture('data', dataTexture)
            .texture('offsets', offsetsTexture)
            .primitive(PicoGL.POINTS)
            .drawRanges([0, N])
            .transformFeedback(
                app
                    .createTransformFeedback()
                    .feedbackBuffer(0, vertexBuffers[0])
            )

        const noninteractingProgram = app.createProgram(
            vnoninteracting.replaceAll('MAX_TEXTURE_SIZE', MAX_TEXTURE_SIZE),
            fnoop,
            {
                transformFeedbackVaryings: ['v_data'],
            }
        )
        const noninteractingCall = app
            .createDrawCall(noninteractingProgram, app.createVertexArray())
            .uniform('N', N)
            .uniform('size', [W, H])
            .uniform('noise', this.noise)
            .texture('data', dataTexture)
            .texture('offsets', offsetsTexture)
            .primitive(PicoGL.POINTS)
            .drawRanges([0, N])
            .transformFeedback(
                app
                    .createTransformFeedback()
                    .feedbackBuffer(0, vertexBuffers[0])
            )

        Object.assign(this, {
            app,
            canvas,
            drawCall,
            fadeCall,
            alignCall,
            noninteractingCall,
            vertexBuffers,
            dataTexture,
            offsetsTexture,
            offsetsArray,
            dataArray,
            MAX_TEXTURE_SIZE,
        })

        this.setTrails()
        this.dispatchEvent(configEvent)
    }, // setup()

    setSize() {
        const canvas = this.canvas
        canvas.width = canvas.height = canvas.clientHeight
        this.app.resize(canvas.width, canvas.height)
        this.setPointSize()
    }, // setSize()

    setPointSize(ps) {
        ps = ps || this.pointSize
        this.drawCall.uniform('pointSize', (ps * this.canvas.width) / this.W)
        this.pointSize = ps
        if (useStorage) localStorage.setItem('pointSize', ps)
    }, // setPointSize()

    setTrails(arg) {
        if (arg !== undefined) this.paintTrails = !!arg
        if (useStorage) {
            localStorage.setItem('paintTrails', this.paintTrails)
        }
    },

    update() {
        const {
            N,
            W,
            H,
            app,
            vertexBuffers,
            dataArray,
            dataTexture,
            offsetsTexture,
            offsetsArray,
        } = this
        const gl = app.gl

        if (this.paintTrails) this.fadeCall.draw()
        else app.clear()
        this.drawCall.draw().gl.flush()

        const tmpData = dataArray.subarray(4 * N, 8 * N)

        gl.bindBuffer(gl.TRANSFORM_FEEDBACK_BUFFER, vertexBuffers[1].buffer)
        gl.getBufferSubData(
            gl.TRANSFORM_FEEDBACK_BUFFER,
            0,
            tmpData // copy straight into the scratch buffer
        )
        gl.bindBuffer(gl.TRANSFORM_FEEDBACK_BUFFER, null)
        // sort - how? avoid generating new arrays

        // *** attempt a different approach
        {
            offsetsArray.fill(0)
            for (let i = 0; i < N; ++i) {
                const cell = tmpData[4 * i + 3]
                ++offsetsArray[cell]
            }

            for (let i = 0, M = W * H, cumsum = 0; i < M; ++i) {
                if (!offsetsArray[i]) offsetsArray[i] = 0xffffffff
                else {
                    const tmp = offsetsArray[i]
                    offsetsArray[i] = cumsum
                    cumsum += tmp
                }
            }
            const insertPositions = Uint32Array.from(
                offsetsArray.subarray(0, W * H)
            )
            for (let i = 0; i < N; ++i) {
                const cell = tmpData[4 * i + 3]
                const idx = insertPositions[cell]
                dataArray.set(tmpData.subarray(4 * i, 4 * i + 4), 4 * idx)
                ++insertPositions[cell]
            }
        }

        dataTexture.data(
            dataArray.subarray(0, 4 * dataTexture.width * dataTexture.height)
        ) // dataTexture is sorted by cells

        offsetsTexture.data(offsetsArray)

        // apply alignCall and write results back to vertexBuffers[0]
        gl.enable(gl.RASTERIZER_DISCARD)
        if (!this.interacting)
            this.noninteractingCall.uniform('seed', Math.random()).draw()
        else this.alignCall.uniform('seed', Math.random()).draw()
        
        gl.disable(gl.RASTERIZER_DISCARD)
        gl.bindBuffer(gl.TRANSFORM_FEEDBACK_BUFFER, null)

        ++this.totalUpdates
    }, // update()

    initialConditions() {
        const { N, W, H, MAX_TEXTURE_SIZE } = this
        const dataSize =
            8 * max(N, (((N / MAX_TEXTURE_SIZE) | 0) + 1) * MAX_TEXTURE_SIZE)
        const data = new Float32Array(dataSize)
        for (let i = 0; i < N; ++i) {
            data.set(
                [random() * W, random() * H, (random() - 0.5) * TWOPI],
                4 * i
            )
        }
        return data
    }, // initialConditions()

    run(ms) {
        if (!this.runID) {
            this.timestamp = ms
            this.updateCount = 0
        } else if (ms - this.timestamp > this.updateInterval) {
            this.timestamp = performance.now()
            this.update()
            ++this.updateCount
        }
        this.runID = requestAnimationFrame(ms => this.run(ms))
    }, // run()

    stop() {
        cancelAnimationFrame(this.runID)
        clearInterval(this.statsUpdateID)
        this.runID = this.lastStatsUpdate = null
    },

    toggleRun() {
        if (this.runID) {
            this.stop()
        } else {
            this.run(performance.now())
            this.statsUpdateID = setInterval(() => {
                if (!this.lastStatsUpdate) {
                    this.lastStatsUpdate = performance.now()
                    this.updateCount = 0
                } else {
                    const dt = performance.now() - this.lastStatsUpdate
                    this.hz = (this.updateCount / dt) * 1e3
                    this.updateCount = 0
                    this.lastStatsUpdate = performance.now()
                    this.dispatchEvent(statsEvent)
                }
            }, this.statsUpdateInterval)
        }
    },

    isRunning() {
        return !!this.runId
    },

    setNumber(num) {
        this.stop()
        this.setup(this.canvas, { N: num })
        if (useStorage) localStorage.setItem('N', num)
    },

    setDimensions(w, h) {
        this.stop()
        this.setup(this.canvas, { W: w, H: h })
        if (useStorage) {
            localStorage.setItem('W', w)
            localStorage.setItem('H', h)
        }
    },

    setNoise(ampl) {
        this.noise = ampl
        this.alignCall.uniform('noise', this.noise)
        if (useStorage) localStorage.setItem('noise', ampl)
    },

    setSpeed(v) {
        this.speed = v
        this.drawCall.uniform('speed', this.speed)
        if (useStorage) localStorage.setItem('speed', v)
    },

    getOrderParameter() {
        const { N, dataArray } = this
        const arr = dataArray.subarray(0, 4 * N)
        let c = 0,
            s = 0
        for (let i = 0; i < N; ++i) {
            const th = arr[4 * i + 2]
            c += cos(th)
            s += sin(th)
        }
        c /= N
        s /= N
        return hypot(s, c)
    },

    getClumpingParameter() {
        const { N, dataArray, vertexBuffers } = this
        const gl = this.app.gl
        const tmpData = dataArray.subarray(4 * N, 8 * N)
        gl.bindBuffer(gl.TRANSFORM_FEEDBACK_BUFFER, vertexBuffers[0].buffer)
        gl.getBufferSubData(
            gl.TRANSFORM_FEEDBACK_BUFFER,
            0,
            tmpData // copy straight into the scratch buffer
        )
        let avgN = 0
        for (let i = 0; i < N; ++i) {
            avgN += tmpData[4 * i + 3]
        }
        return avgN / N - 1
    },

    randomize() {
        let restart = true
        if (this.isRunning()) this.stop()
        else restart = false
        const gl = this.app.gl
        gl.bindTransformFeedback(gl.TRANSFORM_FEEDBACK, null)
        this.vertexBuffers[0].data(
            this.initialConditions().subarray(0, 4 * this.N)
        )
        this.app.clear()
        if (restart) this.start()
    },
})
