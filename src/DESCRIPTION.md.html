<!DOCTYPE html>
<head>
<meta charset="utf-8">
</head>
**Interactive visualization of the Vicsek model**
        Robert J Budzyński (<robert@budzynski.xyz>)

# What is it?

The [Vicsek model](https://en.wikipedia.org/wiki/Vicsek_model) was introduced by 
T.Vicsek et al. [#1] in 1995, as a minimalistic model of 
[active matter](https://en.wikipedia.org/wiki/Active_matter), displaying a "flocking"
behavior. Clearly inspired by the earlier [Boids algorithm](http://www.red3d.com/cwr/boids/)
developed by graphics programmer C. Reynolds in 1986, it was intentionally simplified
by stripping down the rules to the bare minimum that leads to nontrivial
collective behavior -- much in the spirit of the 
[Ising model](https://en.wikipedia.org/wiki/Ising_model), which is paradigmatic 
to statistical physics and the theory of critical phenomena.

The purpose of this simulation is to enable interactive exploration of the model's
behavior in different regions of parameter space, providing a guide for more exhaustive
research by non-visual simulations on larger size systems and over longer time scales.

# Implementation

Currently not only the visualization, but also the dynamical rules of the model 
are implemented for the most part in GL shading language (GLSL), using the 
[WebGL2 API](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API). So the 
motion of the particles is computed mostly on the computer's GPU &mdash; allowing
for surprisingly high performance in terms of the rate of state updates for a large
number of degrees of freedom. On the other hand, the actual performance will vary
significantly depending on the specific GPU. Also, **do not expect the simulation
to perform correctly on mobile device GPUs** &mdash; what exact capabilities are
lacking remains to be investigated.

For the sake of visual experience, the rate of state updates is throttled to 
not exceed 30 Hz -- in practice, it will sometimes be less for sufficiently large 
system size ($N$) (according to the computer's GPU, as remarked above).

For more visual enhancement, the particles are rendered in colors with 
the hue being determined from the particle's direction of motion, and with 
fading-out trails (the latter feature can be disabled). The particle size
can also be tuned by the user, and is given in units of the interaction
radius -- note that this is a purely visual setting
with no impact on the interactions.

The simulation uses the browser's *local storage* to *remember* the user's 
settings, across reloads and sessions. To enable sharing or bookmarking of
*interesting* values of parameters, there is the facility of passing settings
via query parameters in the URL; the current parameter values (or actually,
whatever differs from the fallbacks) can be accessed by a link that copies
those values as a complete link to the system clipboard. Parameter values provided
via query parameters take priority over local storage, otherwise fallback values 
are set. A reset button wipes local storage and return all parameters to their 
fallback values, hard-coded in the simulation.

# Physics

The model's definition contains several free parameters:

- the number of particles $N$ in the system
- an interaction radius $R$
- a speed $v$ which is the constant magnitude of the particles' velocity
- the length scale $W$ of the finite 2D background space
- the amplitude $\eta$ of the random noise perturbing the velocity of particle motion.

Following [#1] this simulation places the particles in a square of size $W$ with
periodic boundary conditions, i.e. a symmetrical torus -- though in principle 
there could be two different length scales consistently with periodicity; 
width and height could be different. A natural intrinsic length scale is provided by the
interaction radius $R$, which I take as the unit of length; the discrete time step
provides the unit of time, thus $v$ is given in units of interaction length 
per time step. The noise amplitude $\eta$ is normalized as a fraction of $2\pi$,
so its maximum value of $1$ corresponds to totally random motion. In this limit,
the Vicsek model reduces to a naive discrete model of Brownian motion, where
in each timestep every particle moves a fixed distance in a random direction.

The initial conditions for the simulation are fully random: the placement of 
the particles is generated from a uniform random distribution over the square,
and the initial velocity directions, from a uniform distribution over the circle.

Note that while the noise level parameter and the particle speed can be adjusted
dynamically on a running (or paused, and later resumed) simulation, changing
the number of particles or system length scale requires reinitialization,
again from a randomized initial state. In principle, this could be avoided -- e.g.
by randomly decimating particles, or adding new particles with random positions
and velocities (in the case of adjustments to $N$), etc., but this hasn't been
implemented. 

The *polarization* parameter which is displayed is defined as the length
of the average velocity vector of all the particles. Its behavior over time 
is plotted dynamically. By choice of units the range of values of this parameter
is $[0, 1]$, with $1$ corresponding to a fully ordered state (all particles
moving in the same direction), and $0$ to totally random motion. Note that
in a finite system, in a disordered state the polarization (a. k. a. heading) 
order parameter is expected to fluctuate above zero, with an expectation value 
of the order of $N^{-1/2}$.

In addition, the system can be seen to undergo various degrees of two other types 
of ordering:

- density inhomogeneity (clumping of particles),
- correlation between position and direction.

These should also be measured by suitable order parameters (work in progress).

As a tentative proposal, the model reports the value of a parameter I purport
to describe clumping, which is the average number of particles within the interaction
radius of a particle (not including the particle itself). It seems to be a 
natural way of describing the overall spatial inhomogeneity of the system's state
at the natural length scale given by the interaction radius &mdash; but clearly
it fails to account for (*e. g.*) long range correlations (among other features).
On the upside, it's calculationally inexpensive to observe and its measurement
imposes negligeable overhead on the simulation.

The expectation value of this parameter for a collection of points distributed 
independently at random according to a uniform distribution is $\pi$ times
the density. Thus the values observed here indicate a high degree of short range
correlations over most of the parameter space, even when the polarization is 
low.

Note that there is nothing in the model to prevent particle clumping -- any 
number of particles can occupy a given volume, they do not repel in any way.

# Remarks

This is a work in progress.

Some inspiration (though not code) was taken from [#8].

# Acknowledgements

This project incorporates the [PicoGL](https://github.com/tsherif/picogl.js/) library,
which eases the drudgery of dealing with the WebGL API, and uses 
[uPlot](https://github.com/leeoniya/uPlot) to draw the plots on the right-side panel.
The current document is formatted with [Markdeep](https://casual-effects.com/markdeep/).


(#) References

[#1]: *Novel Type of Phase Transition in a System of Self-Driven Particles*, Tamás Vicsek et al., https://doi.org/10.1103%2FPhysRevLett.75.1226

[#2]: *New aspects of the continuous phase transition in the scalar noise model (SNM) of collective motion*, M. Nagy, I. Varuka, T. Vicsek, https://dx.doi.org/10.1016/j.physa.2006.05.035

[#3]: *Dry, aligning, dilute, active matter: A synthetic and self-contained overview*,
H. Chaté, B. Mahault, https://arxiv.org/abs/1906.05542

[#4]: *Mutual Information as a Tool for Identifying Phase Transitions in DynamicalComplex Systems with Limited Data*, R. T. Wicks, S. C. Chapman, and R. O. Dendy, https://dx.doi.org/10.1103/PhysRevE.75.051125&v=09e59eab

[#5]: *The Physics of the Vicsek model*, Francesco Ginelli, http://dx.doi.org/10.1140/epjst/e2016-60066-8

[#6]: *Dry Active Matter Exhibits a Self-Organized Cross Sea Phase*, R. Kürsten, T. Ihle, https://doi.org/10.1103/PhysRevLett.125.188003

[#7]: *Phase transitions in swarming systems: A recent debate*, M. Aldana, H. Larralde, B. Vázquez,  https://arxiv.org/ct?url=https%3A%2F%2Fdx.doi.org%2F10.1142%2FS0217979209053552&v=f5b23419

[#8]: https://github.com/allanino/vicsek-model-simulation

<!-- Markdeep: --><style class="fallback">body{visibility:hidden}</style>
<script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js?" charset="utf-8"></script>
