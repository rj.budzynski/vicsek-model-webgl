// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

/** @type {import("snowpack").SnowpackUserConfig } */
import glslify from 'snowpack-plugin-glslify'
export default {
    mount: {
        /* ... */
        src: '/',
    },
    exclude: ['**/shaders/*'],
    plugins: [
        /* ... */
        ['snowpack-plugin-glslify', { compress: true }],
        [
            'snowpack-plugin-minify-html',
            {
                htmlMinifierOptions: {
                    // collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeOptionalTags: true,
                    sortAttributes: true,
                    // minifyCSS: true,
                    // minifyJS: true,
                },
            },
        ],
    ],
    packageOptions: {
        /* ... */
    },
    devOptions: {
        /* ... */
        open: 'none',
    },
    buildOptions: {
        /* ... */
        out: 'public',
        sourcemap: false,
    },
    optimize: {
        bundle: true,
        minify: true,
        target: 'es2018',
    },
}
